# SPI-Soccer-Prediction Version 2:

## Changes
- changing programming language from Python to C# for type safety
- refactored Code

## How to Build Application

### Prerequesites: 
- Visual Studio 2019
- Rapid-Api-Key (instructions on this later)

### Steps to run SPI-Soccer-Prediction:
1. Download the repository
2. Go to (repository-download-location)/ConsoleApp1/Credentials (In Visual Studio or your File-Explorer)
3. Inside of this directory is a cs-file which holds a static property called `footballApiCredentials`
4. Change the value of this property to your Api-Key
5. In case you dont have a Api-Key yet
    1. go to `https://rapidapi.com/api-sports/api/api-football/pricing`
    2. Select the free plan
    3. Sign In or create a new account
    4. Go to `https://rapidapi.com/api-sports/api/api-football/endpoints` and look for your x-rapidapi-key (e.g. located in the displayed code snippet at the right bottom)
6. Build the application by clicking on the green play button in the top-middle of Visual Studio or by hitting the F5-Key inside Visual Studio or by going to Debug->Start Debugging 

# Credits 
Data powered by [API-FOOTBALL](https://www.api-football.com/).
The data provided by [FiveThirtyEight](https://fivethirtyeight.com/) is under Creative Commons Attribution 4.0 International license and is available [here](https://github.com/fivethirtyeight/data/tree/master/soccer-spi).