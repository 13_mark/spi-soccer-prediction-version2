﻿using ConsoleApp1.services;
using ConsoleApp1.Services;
using ConsoleApp1.View;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main()
        {
            var serviceProvider = new ServiceCollection()
            .AddSingleton<IOddsService, OddsService>()
            .AddSingleton<IDataMatcherService, DataMatcherService>()
            .AddSingleton<ISpiDataService, SpiDataService>()
            .BuildServiceProvider();

            var oddsController = new OddsController(serviceProvider.GetService<IOddsService>(), serviceProvider.GetService<IDataMatcherService>());
            var spiDataController = new SpiDataController(serviceProvider.GetService<ISpiDataService>());
            var view = new GameView();

            var gamesToBetOnThisWeek = spiDataController.GetGamesToBetOnThisWeek();

            var chosenMenuOption = view.GetChosenMenuOption();

            if (chosenMenuOption == 1)
            {
                foreach (var game in gamesToBetOnThisWeek)
                {
                    try
                    {
                        game.odd = oddsController.GetBestOddOfGame(game);
                    }
                    catch (Exception)
                    {
                        game.odd = null;
                    }
                }
                view.ShowAllGames(gamesToBetOnThisWeek);
            }

            if (chosenMenuOption == 2)
            {
                var gameId = view.GetGameId();
                var game = spiDataController.GetMinimalBettingOddOfGame(gameId);
                view.ShowGame(game);
            }
        }
    }
}
